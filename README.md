Tout ce que je sais, c'est que je ne sais rien, tandis que les autres croient savoir ce qu'ils ne savent pas.
Existe-t-il pour l'homme un bien plus précieux que la Santé ?
